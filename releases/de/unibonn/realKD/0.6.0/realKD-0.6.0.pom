<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>de.unibonn</groupId>
	<url>https://bitbucket.org/realKD/realkd</url>
	<artifactId>realKD</artifactId>
	<version>0.6.0</version>

	<licenses>
		<license>
			<name>MIT</name>
			<url>https://bitbucket.org/realKD/realkd/downloads/LICENSE_0.1.txt</url>
			<distribution>repo</distribution>
		</license>
	</licenses>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
	</properties>

	<repositories>
		<repository>
			<id>realKD-releases</id>
			<url>https://bitbucket.org/realKD/releases/raw/master/releases</url>
			<releases>
				<enabled>true</enabled>
				<updatePolicy>always</updatePolicy>
				<checksumPolicy>fail</checksumPolicy>
			</releases>
			<snapshots>
				<enabled>true</enabled>
				<updatePolicy>always</updatePolicy>
				<checksumPolicy>warn</checksumPolicy>
			</snapshots>
		</repository>
	</repositories>

	<dependencies>
		<!-- Guava: object utilities -->
		<!-- Apache License 2.0 -->
		<!-- no documented usage restrictions -->
		<dependency>
			<groupId>com.google.guava</groupId>
			<artifactId>guava</artifactId>
			<version>23.0</version>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>com.google.guava</groupId>
			<artifactId>guava-testlib</artifactId>
			<version>23.0</version>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>jline</groupId>
			<artifactId>jline</artifactId>
			<version>2.14.1</version>
		</dependency>

		<!-- test jackson -->
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-databind</artifactId>
			<version>2.9.5</version>
		</dependency>
		<dependency>
			<groupId>com.fasterxml.jackson.datatype</groupId>
			<artifactId>jackson-datatype-guava</artifactId>
			<version>2.9.5</version>
		</dependency>

		<!-- Obtaining annotated classes at runtime -->
		<!-- WTFPL :) -->
		<dependency>
			<groupId>org.reflections</groupId>
			<artifactId>reflections</artifactId>
			<version>0.9.10</version>
		</dependency>

		<!-- Apache Commons Math -->
		<!-- Apache License 2.0 -->
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-math3</artifactId>
			<version>3.6.1</version>
		</dependency>


		<!-- JFreeChart is a class library, written in Java, for generating charts. -->
		<!-- GNU Lesser General Public Licence (LGPL) version 2.1 or later -->
		<dependency>
			<groupId>org.jfree</groupId>
			<artifactId>jfreechart</artifactId>
			<version>1.0.19</version>
		</dependency>

		<!-- A simple library for reading and writing CSV in Java -->
		<!-- Apache License 2.0 -->
		<dependency>
			<groupId>net.sf.opencsv</groupId>
			<artifactId>opencsv</artifactId>
			<version>2.3</version>
		</dependency>


		<!-- GNU General Public License 2.0 -->
		<dependency>
			<groupId>nz.ac.waikato.cms.weka</groupId>
			<artifactId>weka-stable</artifactId>
			<version>3.6.12</version>
		</dependency>
		
		<!-- https://mvnrepository.com/artifact/com.mkobos/pca_transform -->
		<dependency>
		    <groupId>com.mkobos</groupId>
		    <artifactId>pca_transform</artifactId>
		    <version>1.0.2</version>
		</dependency>

		<!-- Common Public License Version 1.0 -->
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.11</version>
		</dependency>

	</dependencies>

	<build>

		<resources>
			<resource>
				<directory>src/main/resources</directory>
				<filtering>true</filtering>
			</resource>
		</resources>

		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
				<version>2.6</version>
				<configuration>
					<archive>
						<manifest>
							<mainClass>de.unibonn.realkd.RealKD</mainClass>
						</manifest>
					</archive>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-source-plugin</artifactId>
				<executions>
					<execution>
						<id>attach-sources</id>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<executions>
					<execution>
						<id>attach-javadocs</id>
						<goals>
							<goal>jar</goal>
						</goals>
						<configuration>
							<additionalparam>-Xdoclint:none</additionalparam>
						</configuration>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-assembly-plugin</artifactId>
				<version>2.4</version>
				<configuration>
					<archive>
						<manifest>
							<mainClass>de.unibonn.realkd.RealKD</mainClass>
						</manifest>
					</archive>
					<descriptorRefs>
						<descriptorRef>jar-with-dependencies</descriptorRef>
					</descriptorRefs>
				</configuration>
				<executions>
					<execution>
						<id>make-jar-with-dependencies</id>
						<phase>package</phase>
						<goals>
							<goal>single</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.6.0</version>
				<configuration>
					<source>1.8</source>
					<target>1.8</target>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>license-maven-plugin</artifactId>
				<version>1.8</version>
				<executions>
					<execution>
						<id>download-licenses</id>
						<goals>
							<goal>download-licenses</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<organizationName>realKD</organizationName>
					<inceptionYear>2014</inceptionYear>
					<licenseName>mit</licenseName>
					<copyrightOwners>Contributors of the realKD Project</copyrightOwners>
					<addJavaLicenseAfterPackage>false</addJavaLicenseAfterPackage>
					<canUpdateCopyright>true</canUpdateCopyright>
					<canUpdateDescription>true</canUpdateDescription>
				</configuration>
			</plugin>

			<plugin>
				<artifactId>maven-resources-plugin</artifactId>
				<version>2.4.3</version>
				<executions>
					<execution>
						<id>copy-resources</id>
						<!-- here the phase you need -->
						<phase>validate</phase>
						<goals>
							<goal>copy-resources</goal>
						</goals>
						<configuration>
							<outputDirectory>${basedir}/target</outputDirectory>
							<resources>
								<resource>
									<directory>src/main/jsh</directory>
									<filtering>true</filtering>
								</resource>
							</resources>
						</configuration>
					</execution>
				</executions>
			</plugin>

		</plugins>
	</build>

	<distributionManagement>
		<repository>
			<id>local.release.repo</id>
			<name>Local release repository</name>
			<!-- Mario:next line assumes that local release repo is next to project 
				folder (from which maven is run). I hope this does not influence non-deploy 
				targets. Let me know if there is any problem with that. In the future maven 
				should directly deploy to remote repo. For that everyone has to have a correctly 
				configured settings.xml -->
			<url>file:../releases/releases/</url>
		</repository>
	</distributionManagement>

</project>
